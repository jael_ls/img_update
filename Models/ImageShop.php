<?php
use Illuminate\Database\Eloquent\Model;

class ImageShop extends Model
{
    protected $table = 'image_shop';
    protected $fillable = ['id_product','id_image','id_shop', 'cover'];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}