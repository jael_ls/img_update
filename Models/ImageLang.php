<?php
use Illuminate\Database\Eloquent\Model;

class ImageLang extends Model
{
    protected $table = 'image_lang';
    protected $fillable = ['id_image','id_lang'];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}