<?php
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
    protected $fillable = ['id_image','id_product', 'position', 'cover'];
    protected $primaryKey = 'id_image';
    public $timestamps = false;
}