<?php
$dir = './img_products/';
$list_id_products = array_slice(scandir($dir), 2);
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update images</title>
</head>
<body>
    <h1>Ce script va remplacer les images actuellement dans la BD par les nouvelles.</h1>
    <p style="color: red">Cette opération est irréversible.</p>
    <a href="./update.php">Lancer le script</a>
    <p>Liste des produits concerné:</p>
    <ul>
        <?php
        foreach ($list_id_products as $id){
            $id = intval($id);
            if ($id){
                echo "<li>".intval($id)."</li>";
            }
        }
        ?>
    </ul>
</body>
</html>
