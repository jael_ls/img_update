<?php
require './bootstrap.php';

$dir = './img_products/';
$list_id_products = scandir($dir);

foreach ($list_id_products as $id_product){

    /*Récupère le produit qui correspond au dossier image*/
    $product = Product::find($id_product);

    /*Si un produit correspond à cette id*/
    if ($product){

        /*Supprimer les images actuelle de la BD ainsi que lignes correspondantes dans image_shop*/
        $images = Image::where('id_product', $id_product)->get();
        $imageShops = ImageShop::where('id_product',$id_product)->delete();

        foreach ($images as $image){
            $image->delete();
        }

        /*Chemin du dossier d'image pour ce produit*/
        $currentDir = $dir . $id_product . '/1200/';
        $listImgName = array_slice(scandir($currentDir), 2);
        $i = 1;

        foreach ($listImgName as $imgName) {

            if ($i == 1){
                /*Créer l'imgage en BD*/
                $image = Image::create([
                    'id_product'    => $id_product,
                    'position'      => $i,
                    'cover'         => 1,
                ]);
                /*Image shop en BD*/
                ImageShop::create([
                    'id_product'    => $id_product,
                    'id_image'      => $image->id_image,
                    'id_shop'       => 1,
                    'cover'         => 1,
                ]);

            } else {
                /*Créer l'imgage en BD*/
                $image = Image::create([
                    'id_product'    => $id_product,
                    'position'      => $i,
                ]);
                /*Image shop en BD*/
                ImageShop::create([
                    'id_product'    => $id_product,
                    'id_image'      => $image->id_image,
                    'id_shop'       => 1,
                ]);

            }
            /*Image lang 1*/
            ImageLang::create([
                'id_image'  => $image->id_image,
                'id_lang'   => 1,
            ]);
            /*Image lang 2*/
            ImageLang::create([
                'id_image'  => $image->id_image,
                'id_lang'   => 2,
            ]);

            $id_image = $image->id_image;

            /*On créé le nom du sous dossier par rapport à l'ID généré*/
            $sousDossier = implode('/', str_split($id_image));

            /*Creation de dossier*/
            if (!mkdir($concurrentDirectory = '../img/p/' . $sousDossier, 0777, true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            /*La destination de l'image*/
            $dest = '../img/p/' . $sousDossier . '/' . $id_image . '.jpg';
            /*L'image source*/
            $source = $currentDir.$imgName;
            /*Copy de l'image*/

            if (!copy($source, $dest)) {

                echo "La copie $source du fichier a échoué...\n";

            }

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-large_default2x.jpg', '1400', '1400', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-large_default.jpg', '700', '700', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-medium_default2x.jpg', '760', '544', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-home_default2x.jpg', '500', '450', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-medium_default.jpg', '380', '272', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-small_default2x.jpg', '300', '300', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-cart_default2x.jpg', '250', '250', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-home_default.jpg', '250', '225', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-small_default.jpg', '150', '150', 100);

            convertImage($source, '../img/p/' . $sousDossier . '/' . $id_image.'-cart_default.jpg', '125', '125', 100);

            $i++;

            echo "<h1 style='text-align: left;color: #28A742;'>Product ".$id_product.": Success -> Image ID: ".$image->id_image."</h1>";
        }

    }
}

function convertImage($source, $dst, $width, $height, $quality){
    $imageSize = getimagesize($source) ;
    $imageRessource= imagecreatefromjpeg($source) ;
    $imageFinal = imagecreatetruecolor($width, $height) ;
    $final = imagecopyresampled($imageFinal, $imageRessource, 0,0,0,0, $width, $height, $imageSize[0], $imageSize[1]) ;
    imagejpeg($imageFinal, $dst, $quality) ;
}
