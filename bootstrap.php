<?php
require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => '127.0.0.1',
    'database'  => 'gaiagwada_preprod',
    'username'  => 'root',
    'password'  => 'root',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => 'ps_',
    'prefix_indexes' => true,
    'strict' => true,
    'engine' => null,
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

