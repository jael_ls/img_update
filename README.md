# Mise à jour des images produit Prestashop
* Placez ou clonez ce dossier à la racine de prestashop
* Faite un composer install dans le dossier img_update
* Mettez vos informations de BD dans le fichier "bootstrap.php"
* Placer vos images dans le dossier img_products et sous dossier nommé avec l'id du produit selon la structure suivante:

    /img_products/"id_product"/1200/votreImage.jpg
    
* Vos images doivent avoir l'extension .jpg
* Rendez-vous sur l'URL https://url_de_votre_site/img_products/
* Suive les instructions.
* Enjoy
